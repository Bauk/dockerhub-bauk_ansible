FROM centos:centos7
#MAINTAINER Kuba Jasko <>

RUN yum -y update && yum clean all
RUN yum -y install openssh-server passwd && yum clean all

RUN mkdir /var/run/sshd
RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N '' 

RUN yum install -y ansible

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

RUN useradd user \
 && echo -e "user\npassword" | (passwd --stdin user)

# EXPOSE 22

