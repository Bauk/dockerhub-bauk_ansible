#!/usr/bin/env sh

COMMAND="/usr/sbin/sshd -D"

function logg {
    printf "ENTRYPOINT: $@\n"
}
function addUser {
    local user=$1
    local pass=$2
    if [[ ! "$pass" ]]
    then
        logg "Using default password: password"
        pass=password
    fi
    useradd $user
    printf "$user\n$pass" | (passwd --stdin user)
}

# Setup a user
if [[ "$USER" ]]
then
    addUser $USER $PASS
fi


$COMMAND
